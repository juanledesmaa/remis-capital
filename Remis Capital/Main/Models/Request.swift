//
//  Request.swift
//  Remis Capital
//
//  Created by juan ledesma on 2/19/19.
//  Copyright © 2019 umbraApp. All rights reserved.
//

import Foundation

struct Request {
    var empresa: String
    var nombre: String
    var celular: String
}
