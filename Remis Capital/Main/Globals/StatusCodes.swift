//
//  StatusCodes.swift
//  Remis Capital
//
//  Created by juan ledesma on 2/19/19.
//  Copyright © 2019 umbraApp. All rights reserved.
//

import Foundation

struct StatusCodes {
    static let ok = 200
}
