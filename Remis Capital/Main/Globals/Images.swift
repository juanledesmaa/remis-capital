//
//  Images.swift
//  Remis Capital
//
//  Created by juan ledesma on 2/19/19.
//  Copyright © 2019 umbraApp. All rights reserved.
//

import Foundation
import UIKit

struct Images {
    
    //MARK:- Icons
    static let refresh  = UIImage(named: "reload")
}
