//
//  DefaultKeys.swift
//  Remis Capital
//
//  Created by juan ledesma on 2/19/19.
//  Copyright © 2019 umbraApp. All rights reserved.
//

import Foundation

struct defaultsKeys {
    static let keyEmpresa = "empresa"
    static let keyCelular = "celular"
    static let keyClave = "clave"
    static let noViaje = ""
}
