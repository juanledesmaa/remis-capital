//
//  JSONToNSData.swift
//  Remis Capital
//
//  Created by juan ledesma on 2/19/19.
//  Copyright © 2019 umbraApp. All rights reserved.
//

import Foundation

func jsonToNSData(json: AnyObject) -> NSData?{
    do {
        return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
    } catch let myJSONError {
        print(myJSONError)
    }
    return nil;
}
