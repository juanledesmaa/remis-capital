//
//  ServiceBaseUrl.swift
//  Remis Capital
//
//  Created by juan ledesma on 2/19/19.
//  Copyright © 2019 umbraApp. All rights reserved.
//

import Foundation

struct ServiceBaseUrl {
    static let baseUrl = "http://pedidos.dataremis.com/v3-osm/"
    static let pushUrl = "http://pedidos.dataremis.com/push/"
}
