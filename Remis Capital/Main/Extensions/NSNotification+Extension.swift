//
//  NSNotification+Extension.swift
//  Remis Capital
//
//  Created by juan ledesma on 2/19/19.
//  Copyright © 2019 umbraApp. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    static let showChatViewController = NSNotification.Name("showChatViewController")
    static let reloadURL = NSNotification.Name("reloadURL")
    static let reloadCompleteURL = NSNotification.Name("reloadCompleteURL")
}
