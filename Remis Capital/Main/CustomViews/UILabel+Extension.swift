//
//  UILabel+Extension.swift
//  Remis Capital
//
//  Created by juan ledesma on 2/19/19.
//  Copyright © 2019 umbraApp. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func makeLoadingAnimation(text: String, stop: Bool) {
        var timer: Timer?
        
        if stop {
            //Stop the timer
            timer?.invalidate()
        } else {
            
            self.text = "\(text) ."
            
            timer = Timer.scheduledTimer(withTimeInterval: 0.55, repeats: true) { (timer) in
                var string: String {
                    switch self.text {
                    case "\(text) .":       return "\(text) .."
                    case "\(text) ..":      return "\(text) ..."
                    case "\(text) ...":     return "\(text) ."
                    default:                return "\(text)"
                    }
                }
                self.text = string
                
            }
        }
    }
    
}
